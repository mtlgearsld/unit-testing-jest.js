const functions = require('./functions');

//toBe
test('Adds 2 + 2 to equal 4', () => {
  expect(functions.add(2, 2)).toBe(4);
});

test('Adds 2 + 2 to NOT equal 5', () => {
  expect(functions.add(2, 2)).not.toBe(5);
});

//to BeNull
test('Should be null', () => {
  expect(functions.isNull()).toBeNull();
});

//to BeFalsy
test('Should be falsy', () => {
  expect(functions.checkValue(null)).toBeFalsy();
});
test('Should be falsy', () => {
  expect(functions.checkValue(0)).toBeFalsy();
});
test('Should be falsy', () => {
  expect(functions.checkValue(undefined)).toBeFalsy();
});

//toEqual
test('User should be John Doe object', () => {
  expect(functions.createUser()).toEqual({
    firstName: 'John',
    lastName: 'Doe',
  });
});

//Less than and greater than
test('should be under 1600', () => {
  const load1 = 800;
  const load2 = 700;
  expect(load1 + load2).toBeLessThan(1600);
});

test('should be under 1600', () => {
  const load1 = 800;
  const load2 = 800;
  expect(load1 + load2).toBeLessThanOrEqual(1600);
});

//REGEX
test('There is no I in team', () => {
  expect('team').not.toMatch(/I/i);
});

//Array
test('Admin should be in usernames', () => {
  usernames = ['john', 'jane', 'admin'];
  expect(usernames).toContain('admin');
});

// Working with async data

//Promise
// test('User fetched name should be Leanne Graham', () => {
//   expect.assertions(1);
//   return functions.fetchUser().then((data) => {
//     expect(data.name).toEqual('Leanne Graham');
//   });
// });

//Async Await
test('User fetched name should be Leanne Graham', async () => {
  expect.assertions(1);
  const data = await functions.fetchUser();
  expect(data.name).toEqual('Leanne Graham');
});
